-- Найти заказ с максимальным количеством купленных цветов, вывести их название и количество
SELECT flowers.name, quantity
FROM clients
         JOIN flowers ON flowers.id = clients.flower_id
WHERE quantity = (SELECT MAX(quantity) FROM clients);

-- Вывести общую выручку (сумму золотых монет по всем заказам) за все время
SELECT SUM(quantity*price) AS sum
FROM clients
    JOIN flowers ON flowers.id = clients.flower_id

-- Получить данные всех заказов одного клиента по идентификатору клиента за последний месяц
SELECT clients_id, flowers.name, quantity, visit_date
FROM clients
         JOIN flowers ON flowers.id = clients.flower_id
         JOIN clientss on clientss.id = clients.clients_id
WHERE visit_date >= (SELECT NOW()::DATE - INTERVAL '1 MONTH')
ORDER BY clients_id;

-- По идентификатору заказа получить данные заказа и данные клиента,  создавшего этот заказ
SELECT clients.id AS order_id, clientss.name, clientss.phone_number, flowers.name, quantity, visit_date
FROM clients
         JOIN flowers ON flowers.id = clients.flower_id
         JOIN clientss on clientss.id = clients.clients_id
ORDER BY order_id;
