create database flowers;

create table flowers
(
    id serial PRIMARY KEY,
    name varchar NOT NULL,
    price INT NOT NULL,
    CONSTRAINT uk_id UNIQUE(id)
);

create table clients
(
    id serial PRIMARY KEY,
    name varchar NOT NULL,
    phone_number varchar NOT NULL
);

create table clients_orders
(
    id serial PRIMARY KEY,
    clients_id BIGINT NOT NULL,
    flower_id BIGINT NOT NULL,
    quantity INT NOT NULL,
    visit_date DATE NOT NULL DEFAULT CURRENT_DATE,
    CONSTRAINT fk_clients_orders_flower_id FOREIGN KEY (flower_id) references flowers(id),
    CONSTRAINT fk_clients_orders_clients_id FOREIGN KEY (clients_id) references clients(id),
    CONSTRAINT ch_quantity CHECK (quantity BETWEEN 1 AND 1000)
);





