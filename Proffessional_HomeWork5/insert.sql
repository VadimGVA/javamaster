insert into flowers (name, price) VALUES ('rose', 200);
insert into flowers (name, price) VALUES ('cactus', 100);
insert into flowers (name, price) VALUES ('peony', 50);
insert into clients (name, phone_number) VALUES ('Andrey', +79139134454);
insert into clients (name, phone_number) VALUES ('Ivan', +79136665214);
insert into clients (name, phone_number) VALUES ('Kirill'), +79132589636);
insert into clients_orders (flower_id, clients_id, quantity, visit_date)
VALUES (6, 7, 30, '2023-03-01');
insert into clients_orders (flower_id, clients_id, quantity, visit_date)
VALUES (3, 4, 6, '2023-03-01');
insert into clients_orders (flower_id, clients_id, quantity, visit_date)
VALUES (2, 3, 12, '2023-04-01');
insert into clients_orders (flower_id, clients_id, quantity, visit_date)
VALUES (2, 4, 58, '2023-04-06');
