package Proffessional_HomeWork2.task4;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Main {
    public static void main(String[] args) {
        ArrayList<Document> documentList = new ArrayList<>();
        documentList.add(new Document.DocumentPlusBuilder()
                .setId(16)
                .setTipe("Справка")
                .setPages(1)
                .build());

        documentList.add(new Document.DocumentPlusBuilder()
                .setId(26)
                .setTipe("Архив")
                .setPages(300)
                .build());

        documentList.add(new Document.DocumentPlusBuilder()
                .setId(74)
                .setTipe("База Данных")
                .setPages(5000)
                .build());

        Map<Integer, Document> documentPlusMap = organizeDocuments(documentList);

        for (Map.Entry<Integer, Document> entry : documentPlusMap.entrySet()) {

            System.out.println("Номер: " + entry.getKey());
            System.out.println("Тип = " + entry.getValue().getTipe());
            System.out.println("Страницы = " + entry.getValue().getPages());
        }
    }

    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> documentMap = new HashMap<>();
        int counter = 1;
        for (Document document : documents) {
            documentMap.put(counter++, document);
        }
        return documentMap;

    }
}