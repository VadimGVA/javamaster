package Proffessional_HomeWork2.task4;

public class Document {

    private int id;
    private String tipe;
    private int pages;

    private Document(DocumentPlusBuilder documentPlusBuilder) {
        id = documentPlusBuilder.id;
        tipe = documentPlusBuilder.tipe;
        pages = documentPlusBuilder.pages;
    }

    public int getId() {
        return id;
    }

    public String getTipe() {
        return tipe;
    }

    public int getPages() {
        return pages;
    }

    public static class DocumentPlusBuilder {
        private int id;
        private String tipe;
        private int pages;

        public DocumentPlusBuilder setId(int id) {
            this.id = id;
            return this;
        }

        public DocumentPlusBuilder setTipe(String tipe) {
            this.tipe = tipe;
            return this;
        }

        public DocumentPlusBuilder setPages(int pages) {
            this.pages = pages;
            return this;
        }

        public Document build() {
            return new Document(this);
        }
    }
}
