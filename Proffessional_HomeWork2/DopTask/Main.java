package Proffessional_HomeWork2.DopTask;


import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;


    public class Main {
        public static void main(String... args) {
            String[] words = new String[]{"the","day","is","sunny","the","the","the","sunny","is","is","day"};
            sortWord(words, 4);

        }

        public static void sortWord(String[] strings, int words) {
            Map<String, Integer> wordsMap = new HashMap<>();
            for (String string : strings) {
                if (wordsMap.containsKey(string)){
                    wordsMap.put(string, wordsMap.get(string) + 1);
                    continue;
                }
                wordsMap.put(string, 1);
            }

            Map<String, Integer> sorted = wordsMap.entrySet()
                    .stream()
                    .limit(words)
                    .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                            (e1, e2) -> e1, LinkedHashMap::new));

            System.out.println(sorted.keySet());
        }
    }





