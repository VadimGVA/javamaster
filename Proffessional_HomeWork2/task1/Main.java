package Proffessional_HomeWork2.task1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        List<Integer> intList = new ArrayList<>();
        intList.add(2);
        intList.add(5);
        intList.add(2);
        intList.add(2);
        intList.add(75);
        intList.add(24);
        intList.add(24);
        intList.add(37);
        intList.add(21);
        intList.add(22);
        intList.add(26);

        originalElements(intList);
    }

    public static <T> void originalElements(List<T> list) {
        Set<T> set = new HashSet<>(list);
        System.out.print(list.getClass().getName() + " - оригинальные значения: ");
        set.forEach((value) -> System.out.print(value + " "));
    }
}





