package Proffessional_HomeWork2.task2;
import java.util.*;


    public class Main {
        public static void main(String[] args) {
            System.out.println("Данная нейросеть проверить два значения, " +
                    "являются ли они анаграммами" + "\n"
                    + "Укажите исходные значения: ");
            Scanner sc = new Scanner(System.in);
            String s = sc.nextLine();
            String t = sc.nextLine();

            System.out.print(checkAn(s, t));
        }

        public static boolean checkAn(String string1, String string2) {
            boolean r = true;
            if (string1.length() != string2.length()) return false;
                        char[] char1 = string1.toCharArray();
            char[] char2 = string2.toCharArray();
            Map<Character, Integer> map1 = new HashMap<>();
            Map<Character, Integer> map2 = new HashMap<>();

            for (Character ch1 : char1) {
                if (map1.containsKey(ch1)) {
                    map1.put(ch1, map1.get(ch1) + 1);
                    continue;
                }
                map1.put(Character.toLowerCase(ch1), 1);
            }

            for (Character ch2 : char2) {
                if (map2.containsKey(ch2)) {
                    map2.put(Character.toLowerCase(ch2), map2.get(ch2) + 1);
                    continue;
                }
                map2.put(ch2, 1);
            }

            for (Map.Entry<Character, Integer> entry : map1.entrySet()) {
                if (!map2.containsKey(entry.getKey()) || !map2.containsValue(entry.getValue())) {
                    r = false;
                    break;
                }
            }
            return r;
        }
    }

