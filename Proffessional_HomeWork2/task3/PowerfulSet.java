package Proffessional_HomeWork2.task3;
import java.util.HashSet;
import java.util.Set;


public class PowerfulSet {

    public <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Set<T> r = new HashSet<>(set1);
        r.retainAll(set2);
        return r;
    }
    public <T> Set<T> union(Set<T> set1, Set<T> set2) {
        Set<T> r2 = new HashSet<>(set1);
        r2.addAll(set2);
        return r2;
    }
    public <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) {
        Set<T> r3 = new HashSet<>(set1);
        r3.removeAll(set2);
        return r3;
    }
}






