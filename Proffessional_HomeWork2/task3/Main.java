package Proffessional_HomeWork2.task3;
import java.util.HashSet;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>();
        Set<Integer> set2 = new HashSet<>();

        set1.add(2);
        set1.add(3);
        set1.add(8);
        set1.add(8);
        set1.add(1);

        set2.add(2);
        set2.add(3);
        set2.add(8);
        set2.add(7);
        set1.add(6);
        set1.add(4);

        PowerfulSet powerfulSet = new PowerfulSet();

        System.out.println(powerfulSet.intersection(set1, set2));

        System.out.println(powerfulSet.union(set1, set2));

        System.out.println(powerfulSet.relativeComplement(set1, set2));

    }
}



