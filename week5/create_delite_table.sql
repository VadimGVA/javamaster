select 1;

create table public.books
(
  id serial primary key,
  title varchar(30) not null,
  autor varchar(30) not null,
  date_added timestamp not null
);


select * from books;

commit ;

insert into books(title, autor, date_added)
values ('Недоросль', 'Д.И. Фомин', now() );

select * from books;