package Proffessional_HomeWork4.task6;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Main {
        public static void main(String[] args) {
            Set<Integer> one = Stream.of(12, 2333, 23).collect(Collectors.toSet());
            Set<Integer> too = Stream.of(41, 55, 16).collect(Collectors.toSet());
            Set<Set<Integer>> initialSet= new HashSet<>();
            initialSet.add(one);
            initialSet.add(too);

            initialSet.forEach(System.out::println);
            Set<Integer> res = initialSet.stream()
                    .flatMap(Collection::stream)
                    .collect(Collectors.toSet());
            System.out.println(res);
        }
    }


