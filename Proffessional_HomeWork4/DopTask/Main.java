package Proffessional_HomeWork4.DopTask;

public class Main {
    public static void main(String[] args) {
        System.out.println("\"cat\" and \"cats\" could be equalize = " + equalize("cat", "cats"));
        System.out.println("\"cat\" and \"cut\" could be equalize = " + equalize("cat", "cut"));
        System.out.println("\"cat\" and \"nut\" could be equalize = " + equalize("cat", "nut"));
    }
    public static boolean equalize(String str1, String str2){
        if (Math.abs(str1.length() - str2.length()) > 1)
            return false;
        else {
            int one = str1.length() - 1;
            int too = str2.length() - 1;
            int res = 0;
            while(res < 2 && one >= 0 && too >= 0) {
                if (str1.charAt(one) != str2.charAt(too)) {
                    if (one > 0 && str1.charAt(one - 1) == str2.charAt(too)) {
                        one--;
                    } else if (too > 0 && str1.charAt(one) == str2.charAt(too - 1)) {
                        too--;
                    }
                    res++;
                }
                one--;
                too--;
            }
            return res < 2;
        }
    }
}


