package Proffessional_HomeWork1.task2;

public class MyUncheckedException extends RuntimeException {
    public MyUncheckedException () {
        super("Непроверяемое исключение");
    }

    public MyUncheckedException(String mess) {
        super(mess);
        System.out.println("LНепроверяемое исключение - " + mess);
    }
}

