package Proffessional_HomeWork1.task4;

public class MyEvenNumber {
    public int n;

    public MyEvenNumber(int n) {
        try {
            if (n % 2 != 0)
                throw new MyEvenNumberException();
            this.n = n;
        } catch (MyEvenNumberException exception) {
            System.out.println(exception.getMessage());
        }
    }
}

