package Proffessional_HomeWork1.task3;
import java.util.Locale;
import java.util.Scanner;
import java.io.FileWriter;
import java.io.File;
import java.io.IOException;
import java.io.Writer;



public class MainClass {

        private static final String PKG_DIRECTORY = "C:\\Users\\gatin\\IdeaProjects\\JavaCourseBasic\\src\\Proffessional\\task3";
        private static final String OUTPUT_FILE_NAME = "output.txt";
        private static final String INPUT_FILE_NAME = "input.txt";

        public static void main(String[] args) {
            try {
                FileReadWrite();
            } catch (IOException e) {
                System.out.println("LOG: " + e.getMessage());
            }
        }
        public static void FileReadWrite() throws IOException {
            Scanner sc = new Scanner(new File(PKG_DIRECTORY + "\\" + INPUT_FILE_NAME));
            Writer writer = new FileWriter(PKG_DIRECTORY + "\\" + OUTPUT_FILE_NAME);
            try (sc; writer) {
                while (sc.hasNext()) {
                    writer.write(sc.nextLine().toUpperCase(Locale.ROOT) + "\n");
                }
            }
        }
    }



