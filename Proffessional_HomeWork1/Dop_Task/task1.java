package Proffessional_HomeWork1.Dop_Task;

import java.util.Scanner;

public class task1 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];
        int index = -1;
        int max = Integer.MIN_VALUE;

        for(int i = 0; i < n ; i++) {
            arr[i] = sc.nextInt();
            if(arr[i] > max) {
                max = arr[i];
                index = i;
            }
        }

        System.out.print(max + " ");
        max = Integer.MIN_VALUE;

        for(int i = 0; i < n ; i++) {
            if(i == index) continue;
            if(arr[i] > max) {
                max = arr[i];
            }
        }
        System.out.print(max);
    }
}

