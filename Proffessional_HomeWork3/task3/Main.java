package Proffessional_HomeWork3.task3;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Constructor;

public class Main {
    public static void main(String[] args) {

        Class<APrinter> clazz = APrinter.class;

        try {
            Constructor<APrinter> constructor = clazz.getDeclaredConstructor();
            APrinter result = constructor.newInstance();
            Method method = result
                    .getClass()
                    .getDeclaredMethod("print", int.class);


            method.invoke(result, 5);
        } catch (IllegalArgumentException ex) {
            System.out.println("Incorrect arguments given");
        } catch (NoSuchMethodException e) {
            System.out.println("No such method...");
        } catch (IllegalAccessException e) {
            System.out.println("The method access modifiers forbid calling it...");
        } catch (InstantiationException e) {
            System.out.println("Class is abstract...");
        } catch (InvocationTargetException e) {
            System.out.println("The method thrown an exception...");
        }
    }
}

