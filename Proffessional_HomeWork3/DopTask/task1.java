package Proffessional_HomeWork3.DopTask;

import java.util.Stack;

public class task1 {
    public static void main(String[] args) {
        System.out.println(control("(()"));
        System.out.println(control("((()))"));
        System.out.println(control(")("));

    }

    public static boolean control(String string) {
        if (string.length() % 2 != 0)
            return false;

        Stack<Character> result = new Stack<>();

        for (int i = 0; i < string.length(); i++) {
            char symbol = string.charAt(i);
            if (symbol == '(')
                result.push(symbol);
            else if (symbol == ')' && !result.isEmpty())
                result.pop();
            else
                return false;
        }

        return result.isEmpty();
    }
}

