package Proffessional_HomeWork3.task2;

import Proffessional_HomeWork3.task1.IsLike;
import Proffessional_HomeWork3.task1.Main;

public class Test {
    public static void main(String[] args) {
        getLike(Main.class);

    }

    public static void getLike(Class<?> clazz){
        if (!clazz.isAnnotationPresent(IsLike.class)) {
            System.out.println("В указанном классе анотации отсутствуют");
            return;
        }

        IsLike isLike = clazz.getAnnotation(IsLike.class);

        System.out.println("isLikeInfo = " + isLike.isLike());
    }
}
