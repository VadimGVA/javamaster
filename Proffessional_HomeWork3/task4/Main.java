package Proffessional_HomeWork3.task4;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

public class Main {
        public static void main(String[] args) throws IllegalAccessException,
                InvocationTargetException, NoSuchMethodException, InstantiationException {
            List<String> interfaces = new ArrayList<>();
            for(Class<?> clazz : getInter(First.class)){
                System.out.println(clazz.getSimpleName());
            }
        }

        public static Class<?>[] getInter(Class<?> clazz) {
            Set<Class<?>> allInterfaces = new HashSet<>();

            Set<Class<?>> allClasses = new HashSet<>();
            Class<?> baseClass = clazz;
            while (baseClass != null) {
                allClasses.add(baseClass);
                baseClass = baseClass.getSuperclass();
            }
            for (Class<?> cls : allClasses) {
                for (int i = 0; i < cls.getInterfaces().length ; i++) {
                    allInterfaces.add(cls.getInterfaces()[i]);
                    allInterfaces.addAll(List.of(getInter(cls.getInterfaces()[i])));
                }
            }

            Class<?>[] result = new Class<?>[allInterfaces.size()];
            int i = 0;
            for (Class<?> value : allInterfaces) {
                result[i++] = value;
            }
            return result;
        }
    }





